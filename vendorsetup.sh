# Kernel Tree
echo 'Cloning Kernel tree [1/3]'
git clone https://github.com/ashim-anwar/kernel_realme_RMX1901.git -b x.337-old kernel/realme/RMX1901

# Vendor Tree
echo 'Cloning Vendor tree [2/3]'
git clone git@gitlab.com:firedroid/vendor_realme_rmx1901.git -b 14 vendor/realme/RMX1901

# Realme In-Screen Proximity Patch
echo 'Patching Realme In-Screen Proximity [3/3]'
cd frameworks/base && git fetch git@github.com:ashim-anwar/proximity_patch.git 13 && git cherry-pick 30ea88924980002d94fb8e2bf4a5016905ca6914 && cd ../..